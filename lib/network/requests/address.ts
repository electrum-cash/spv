// Import electrum provider to use in all requests.
import { networkProvider } from '../provider';

// Import utility functions.
import { validateElectrumProvider, generateTokenFilter } from './utilities';

// ...
import type
{
	// Primitives
	Address,
	BlockHeight,

	// Address related requests.
	AddressGetBalanceResponse,
	AddressGetHistoryResponse,
	AddressGetMempoolResponse,
	AddressListUnspentResponse,
} from '../../interfaces';

/**
 * @module Address
 * @memberof Network
 */

/**
 * Fetches a balance for a given address.
 * @group Requests
 *
 * @param address {Address}         - The address to fetch balance for.
 * @param includeSatoshis {boolean} - If the balance should include outputs that does not have tokens on them.
 * @param includeTokens {boolean}   - If the balance should include outputs that have tokens on them.
 *
 * @returns {AddressGetBalanceResponse} the number of confirmed and unconfirmed satoshis on the relevant outputs.
 */
export async function fetchBalance(address: Address, includeSatoshis: boolean = true, includeTokens: boolean = false): Promise<AddressGetBalanceResponse>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchTrustedBalance');

	// Construct the token filter to determine what balances to include.
	const tokenFilter = await generateTokenFilter(includeSatoshis, includeTokens);

	// Fetch the transaction from the network.
	const trustedAddressBalance = await networkProvider.request('blockchain.address.get_balance', address, tokenFilter) as AddressGetBalanceResponse | Error;

	// Throw an error if the request failed.
	if(trustedAddressBalance instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = trustedAddressBalance.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch trusted balance: ${errorMessage}`));
	}

	return trustedAddressBalance;
}

/**
 * Fetches the transaction history for an address.
 * @group Requests
 *
 * @param address {Address}               - The address to fetch transaction history for.
 * @param fromHeight {BlockHeight}        - Limit transactions to those included in this and later blocks, if provided.
 * @param toHeightExclusive {BlockHeight} - Limit transactions to those included before this block, if provided.
 *
 * @returns {AddressGetHistoryResponse} the transactions that make up the on-chain history requested.
 */
export async function fetchHistory(address: Address, fromHeight: BlockHeight = 0, toHeightExclusive: any = -1): Promise<AddressGetHistoryResponse>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchHistory');

	// Fetch the history from the network.
	const addressHistory = await networkProvider.request('blockchain.address.get_history', address, fromHeight, toHeightExclusive) as AddressGetHistoryResponse | Error;

	// Throw an error if the request failed.
	if(addressHistory instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = addressHistory.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch address history: ${errorMessage}`));
	}

	return addressHistory;
}

/**
 * Fetches a list of transactions related to an address, that are currently unconfirmed in the mempool.
 * @group Requests
 *
 * @param address {Address} - The address to fetch mempool transactions for.
 *
 * @returns {AddressGetMempoolResponse} the list of related transactions in the mempool.
 */
export async function fetchPendingTransactions(address: Address): Promise<AddressGetMempoolResponse>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchPendingTransactions');

	// Fetch the transactions from the network.
	const pendingTransactions = await networkProvider.request('blockchain.address.get_mempool', address) as AddressGetMempoolResponse | Error;

	// Throw an error if the request failed.
	if(pendingTransactions instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = pendingTransactions.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch pending transactions: ${errorMessage}`));
	}

	return pendingTransactions;
}

/**
 * Fetches a list of transaction outputs that are ready to be spent.
 * @group Requests
 *
 * @param address {Address} - The address to fetch a list of unspent transaction outputs for.
 * @param includeSatoshis {boolean} - If the list should include outputs that does not have tokens on them.
 * @param includeTokens {boolean}   - If the list should include outputs that have tokens on them.
 *
 * @return {AddressListUnspentResponse} a list of unspent transaction outputs.
*/
export async function fetchUnspentTransactionOutputs(address: Address, includeSatoshis: boolean = true, includeTokens: boolean = false): Promise<AddressListUnspentResponse>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchUnspentTransactionOutputs');

	// Construct the token filter to determine what outputs to include.
	const tokenFilter = await generateTokenFilter(includeSatoshis, includeTokens);

	// Fetch the list of unspent outputs from the network.
	const unspentOutputs = await networkProvider.request('blockchain.address.listunspent', address, tokenFilter) as AddressListUnspentResponse | Error;

	// Throw an error if the request failed.
	if(unspentOutputs instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = unspentOutputs.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch unspent outputs: ${errorMessage}`));
	}

	return unspentOutputs;
}

/**
 * @group Requests
 *
 */
export async function subscribeToAddressUpdates(address: Address): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('subscribeToAddressUpdates');

	// Subscribe to status updates for the address.
	await networkProvider.subscribe('blockchain.address.subscribe', address);
}

/**
 * @group Requests
 *
 */
export async function unsubscribeFromAddressUpdates(address: Address): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('unsubscribeFromAddressUpdates');

	// Unsubscribe to status updates for the address.
	await networkProvider.subscribe('blockchain.address.unsubscribe', address);
}
