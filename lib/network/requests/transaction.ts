// Import electrum provider to use in all requests.
import { networkProvider } from '../provider';

// Import utility functions.
import { validateElectrumProvider } from './utilities';

// ...
import type
{
	// Primitives
	TransactionHex,
	TransactionHash,
	BlockHeight,

	// Transaction related requests.
	TransactionBroadcastResponse,
	TransactionGetResponse,
	TransactionGetMerkleResponse,
	TransactionGetHeightResponse,
	TransactionDoublespendProofGetResponse,
} from '../../interfaces';

/**
 * @module Transactions
 * @memberof Network
 */

/**
 * TODO: (later/never?)
 * blockchain.transaction.id_from_pos
 * blockchain.transaction.dsproof.list
 */

/**
 * Broadcasts a raw transaction to the network.
 * @group Abstractions
 *
 * @param transactionHex {transactionHex} - the raw transaction to broadcast, as a hex-encoded string.
 *
 * @returns {Promise<TransactionHash>} the transactionHash of the broadcasted transaction.
 */
export async function broadcastTransaction(transactionHex: TransactionHex): Promise<TransactionHash>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('broadcastTransaction');

	// Fetch the transaction from the network.
	const transactionHash = await networkProvider.request('blockchain.transaction.broadcast', transactionHex) as TransactionBroadcastResponse | Error;

	// Throw an error if the request failed.
	if(transactionHash instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = transactionHash.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to broadcast transaction '${transactionHex}': ${errorMessage}`));
	}

	return transactionHash;
}

/**
 * Fetches a transaction from the network.
 * @group Requests
 *
 * @param transactionHash    {transactionHash}   hash of the transaction to fetch, as a hex-encoded string.
 *
 * @returns {Promise<TransactionHex>} the transaction as a hex-encoded string.
 */
export async function fetchTransaction(transactionHash: TransactionHash): Promise<TransactionHex>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchTransaction');

	// Fetch the transaction from the network.
	const transactionHex = await networkProvider.request('blockchain.transaction.get', transactionHash) as TransactionGetResponse | Error;

	// Throw an error if the request failed.
	if(transactionHex instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = transactionHex.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch transaction '${transactionHash}': ${errorMessage}`));
	}

	return transactionHex;
}

/**
 * Fetches the block height that a given transaction was included in, or a number indicating it is present in the mempool
 * @group Requests
 *
 * @param transactionHash    {TransactionHash}            hash of the transaction to fetch a block height for.
 *
 * @returns {Promise<BlockHeight>} the block height the transaction was included in, if available.
 */
export async function fetchTransactionBlockHeight(transactionHash: TransactionHash): Promise<BlockHeight>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchTransactionBlockHeight');

	// Fetch the block height of the transaction from the backend network servers.
	const blockHeight = await networkProvider.request('blockchain.transaction.get_height', transactionHash) as TransactionGetHeightResponse | Error;

	// Throw an error if the request failed.
	if(blockHeight instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = blockHeight.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch transaction confirmation height: ${errorMessage}`));
	}

	// Return the block height the transaction was included in, or it's presence in the mempool.
	return blockHeight;
}

/**
 * Fetches the transaction's merkle proof from the network.
 * @group Requests
 *
 * @param transactionHash    {TransactionHash}   hex-encoded string of the transaction hash
 * @param blockHeight        {BlockHeight}       number for the blockheight that transaction was stored in
 *
 * @throws if the network returns an invalid merkle proof.
 * @returns {Promise<TransactionGetMerkleResponse | undefined>} the merkle proof for the transaction, or undefined if transaction is still unconfirmed.
 */
export async function fetchTransactionProof(transactionHash: TransactionHash, blockHeight: BlockHeight): Promise<TransactionGetMerkleResponse | undefined>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchTransactionProof');

	// Fetch the proof from the backend network servers.
	const transactionInclusionProof = await networkProvider.request('blockchain.transaction.get_merkle', transactionHash, blockHeight) as TransactionGetMerkleResponse | Error;

	// Throw an error if the request failed.
	if(transactionInclusionProof instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = transactionInclusionProof.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch transaction inclusion proof: ${errorMessage}`));
	}

	return transactionInclusionProof;
}

/**
 * TODO: Document me.
 * @group Requests
 */
export async function subscribeToTransactionUpdates(transactionHash: TransactionHash): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('subscribeToTransactionUpdates');

	// Subscribe to status updates for the transaction.
	await networkProvider.subscribe('blockchain.transaction.subscribe', transactionHash);
}

/**
 * TODO: Document me.
 * @group Requests
 */
export async function unsubscribeFromTransactionUpdates(transactionHash: TransactionHash): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('unsubscribeFromTransactionUpdates');

	// Unsubscribe to status updates for the transaction.
	await networkProvider.subscribe('blockchain.transaction.unsubscribe', transactionHash);
}

/**
 * TODO: Document me.
 * @group Requests
 */
export async function fetchDoublespendProof(transactionHash: TransactionHash): Promise<TransactionDoublespendProofGetResponse>
{
	// blockchain.transaction.dsproof.get
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchDoublespendProof');

	// Fetch the transaction doublespend proof from the network.
	const doublespendProof = await networkProvider.request('blockchain.transaction.dsproof.get', transactionHash) as TransactionDoublespendProofGetResponse | Error;

	// Throw an error if the request failed.
	if(doublespendProof instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = doublespendProof.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch doublespend proof for '${transactionHash}': ${errorMessage}`));
	}

	return doublespendProof;
}

/**
 * TODO: Document me.
 * @group Requests
 */
export async function subscribeToDoublespendUpdates(transactionHash: TransactionHash): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('subscribeToDoublespendUpdates');

	// Subscribe to doublespend updates for the transaction.
	await networkProvider.subscribe('blockchain.transaction.dsproof.subscribe', transactionHash);
}

/**
 * TODO: Document me.
 * @group Requests
 */
export async function unsubscribeFromDoublespendUpdates(transactionHash: TransactionHash): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('unsubscribeFromDoublespendUpdates');

	// Unsubscribe to doublespend updates for the transaction.
	await networkProvider.subscribe('blockchain.transaction.dsproof.unsubscribe', transactionHash);
}
