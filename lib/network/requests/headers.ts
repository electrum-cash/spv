// Import electrum provider to use in all requests.
import { networkProvider } from '../provider';

// Import utility functions.
import { validateElectrumProvider } from './utilities';

// ...
import type
{
	// Primitives
	BlockHeight,
	BlockHeaderHex,
	BlockHeaderHash,

	// Block related requests.
	BlockHeaderResponse,
	BlockHeaderWithProofResponse,
	BlockHeadersResponse,

	// Header related requests.
	HeadersGetTipResponse,
} from '../../interfaces';

/**
 * @module Blockchain
 * @memberof Network
 */

/**
 * Fetches the current block header for a given block height from the network.
 * @group Requests
 *
 * @param blockHeight        {BlockHeight}            block height to get block header for.
 *
 * @returns {Promise<BlockHeaderHex>} the block header as a hex-encoded string.
 */
export async function fetchBlockHeaderFromBlockHeight(blockHeight: BlockHeight): Promise<BlockHeaderHex>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchBlockHeaderFromBlockHeight');

	// Fetch the block header from the backend network servers.
	const blockHeaderHex = await networkProvider.request('blockchain.block.header', blockHeight) as BlockHeaderResponse | Error;

	// Throw an error if the request failed.
	if(blockHeaderHex instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = blockHeaderHex.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch block header from block height: ${errorMessage}`));
	}

	// Return the block header.
	return blockHeaderHex;
}

/**
 * Fetches the current block header with an inclusion proof for a given block height from the network.
 * @group Requests
 *
 * @param blockHeight        {BlockHeight}            block height to get block header for.
 * @param checkpointHeight   {BlockHeight}            block height of the checkpoint to use as the base of the inclusion proof.
 *
 * @returns {Promise<BlockHeaderWithProofResponse>} the block header with inclusion proof.
 */
export async function fetchBlockHeaderWithProofFromBlockHeight(blockHeight: BlockHeight, checkpointHeight: BlockHeight): Promise<BlockHeaderWithProofResponse>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchBlockHeaderWithProofFromBlockHeight');

	// Fetch the block header with inclusion proof from the backend network servers.
	const blockHeaderWithProof = await networkProvider.request('blockchain.block.header', blockHeight, checkpointHeight) as BlockHeaderWithProofResponse | Error;

	// Throw an error if the request failed.
	if(blockHeaderWithProof instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = blockHeaderWithProof.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch block header with proof from block height: ${errorMessage}`));
	}

	// Return the block header with the inclusion proof.
	return blockHeaderWithProof;
}

/**
 * Fetches the one or more block headers starting at a given height.
 * @group Requests
 *
 * @param startingBlockHeight   {BlockHeight}       block height for the first block header to request.
 * @param count                 {number}            the maximum number of block headers to request.
 *
 * @returns {Promise<Array<BlockHeaderHex>>} a list of block headers.
 */
export async function fetchBlockHeaders(startingBlockHeight: BlockHeight, count: number): Promise<Array<BlockHeaderHex>>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchBlockHeaders');

	// Fetch the block header with inclusion proof from the backend network servers.
	const response = await networkProvider.request('blockchain.block.headers', startingBlockHeight, count) as BlockHeadersResponse | Error;

	// Throw an error if the request failed.
	if(response instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = response.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch up to ${count} block headers starting with #${startingBlockHeight}: ${errorMessage}`));
	}

	// Extract the concatenated block headers.
	const concatenatedBlockHeaders = response.hex;

	// Initialize an empty list of block headers.
	const blockHeaders: Array<BlockHeaderHex> = [];

	// TODO: Move this to a constant somewhere.
	const blockHeaderLength = 80 * 2;

	// Extract each block header hash and push them into the list.
	for(let index = 0; index < response.count; index += 1)
	{
		blockHeaders.push(concatenatedBlockHeaders.slice(index * blockHeaderLength, (index + 1) * blockHeaderLength));
	}

	// Return the list of block headers
	return blockHeaders;
}

/**
 * Fetches the current chain tip from the network.
 * @group Requests
 *
 * @returns {Promise<HeadersGetTipResponse>} the current block height and block hash of the current chain tip.
 */
export async function fetchCurrentChainTip(): Promise<HeadersGetTipResponse>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('fetchCurrentChainTip');

	// Fetch the current chain tip.
	const currentChainTip = await networkProvider.request('blockchain.headers.get_tip') as HeadersGetTipResponse | Error;

	// Throw an error if the request failed.
	if(currentChainTip instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = currentChainTip.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch the current chain tip: ${errorMessage}`));
	}

	// Return the fetched chain tip.
	return currentChainTip;
}

/**
 * Subscribes to new block headers.
 * @group Requests
 *
 * @note before calling a subscription related method, you should set up an event listener to handle the generated notifications.
 */
export async function subscribeToBlockheaderUpdates(): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('subscribeToBlockheaderUpdates');

	// Subscribe to new block headers.
	await networkProvider.subscribe('blockchain.headers.subscribe');
}

/**
 * Unsubscribes from new block headers.
 * @group Requests
 */
export async function unsubscribeFromBlockheaderUpdates(): Promise<void>
{
	// Validate that the electrum provider has been initialized.
	await validateElectrumProvider('unsubscribeFromBlockheaderUpdates');

	// Unsubscribe from new block headers.
	await networkProvider.unsubscribe('blockchain.headers.subscribe');
}
