// Import types.
import type { VersionNumbers } from '../interfaces';

/**
 * Extracts the major, minor and patch numbers from an electrum version string.
 */
async function extractVersionNumbers(versionString: string): Promise<VersionNumbers>
{
	// Define a regexp to extract the version numbers.
	const regexp = /(?<major>[0-9]+)(\.(?<minor>[0-9]+))?(\.(?<patch>[0-9]+))?/g;

	// Consume the iterator to get the single match.
	const { value: match } = versionString.matchAll(regexp).next();

	// Throw an error if we couldn't parse the version numbers.
	if(!match || !match.groups.major)
	{
		throw(new Error(`Could not extract version numbers from '${versionString}'.`));
	}

	// Return the extracted version numbers.
	return match.groups as unknown as VersionNumbers;
}

/**
 * Verifies that a provided electrum version string is sufficient given a required version.
 */
export async function hasSufficientVersion(providedVersionString: string, requiredVersionString: string): Promise<boolean>
{
	const providedVersionNumbers = await extractVersionNumbers(providedVersionString);
	const requiredVersionNumbers = await extractVersionNumbers(requiredVersionString);

	// Return true if the provided major version exceeds required major version.
	if(providedVersionNumbers.major > requiredVersionNumbers.major)
	{
		return true;
	}

	// Return false if the provided major version is insufficient.
	if(providedVersionNumbers.major < requiredVersionNumbers.major)
	{
		return false;
	}

	// The major version is now the same.

	// Return true if the provided major version is the same, and the minor version exceeds required minor version.
	if(providedVersionNumbers.minor > requiredVersionNumbers.minor)
	{
		return true;
	}

	// Return false if the provided major version is insufficient.
	if(providedVersionNumbers.minor < requiredVersionNumbers.minor)
	{
		return false;
	}

	// The major and minor versions are now the same.

	// Return true if the provided major and minor versions are the same, and the patch version is sufficient.
	if(providedVersionNumbers.patch >= requiredVersionNumbers.patch)
	{
		return true;
	}

	// Return false since the patch version was not sufficient.
	return false;
}
