// Import network access from electrum-cash.
import { electrumServers as defaultServers } from '@electrum-cash/servers';
import { electrumCheckpoint as defaultCheckpoint } from '@electrum-cash/checkpoint';

//
import { EventEmitter } from 'events';

// Import electrum network abstractions
import
{
	networkProvider,
	initializeNetworkProvider,
	fetchBlockHeaders,
	fetchHistory,
	fetchTransactionBlockHeight,
	subscribeToBlockheaderUpdates,
	subscribeToAddressUpdates,
	subscribeToTransactionUpdates,
	subscribeToDoublespendUpdates,
} from './network';

// ..
import { storeInCache, getFromCache } from './cache';
import { getBlockHashFromHeader, getMerkleRootFromHeader } from './headers';
import { getTransaction, getTransactionInclusionProof, getBlockHeaderFromHeight } from './data';
import { verifyTransactionInclusionProof } from './chain';

// Import required type definitions.
import type { TransportScheme } from '@electrum-cash/network';
import type { ElectrumServers } from '@electrum-cash/servers';

// Import electrum and blockchain related type information.
import type { ElectrumNotification, BlockHeight, BlockHeaderHex, HeadersSubscribeNotification, Address, AddressStatus, AddressGetHistoryResponse, AddressSubscribeNotification, TransactionHash, TransactionSubscribeNotification, TransactionDsProofNotification } from './interfaces';

// Import thread locking library to control order or operations.
import { Mutex } from 'async-mutex';

/**
 * @module ElectrumSPV
 */

// ...
// TODO: Move into separate module and expose via get/set functions.
const TRANSACTION_IS_UNKNOWN = 0;
const TRANSACTION_IS_RECEIVED = 1;
const TRANSACTION_IS_DOUBLESPENT = 2;
const TRANSACTION_IS_VERIFIED = 3;

// Initialize an empty in-memory transaction state tracker.
// TODO: Move into separate module and expose via get/set functions.
const internalTransactionState: Record<TransactionHash, number> = {};

// Initialize a mutex lock for the synchronization of block headers.
const headerSynchronizationLock = new Mutex();

// Initialize a default empty chain status.
let currentChainStatus = { verifiedHeight: 0, verifiedPercent: 0 };

/**
* @event ChainStatus
* @group Events
*
* @property {number} verifiedHeight  - number of blocks that been verified to be part of the block chain.
* @property {number} verifiedPercent - percentage of blocks that have been verified to be included in the chain.
*/

/**
* @event BlockReceived
* @group Events
*
* @property {BlockHeaderHash} blockHash - hash of the received blocks header.
* @property {BlockHeight} blockHeight - height at which the block was verified to be included in the chain.
*/

/**
* @event BlockVerified
* @group Events
*
* @property {BlockHeaderHash} blockHash   - hash of the verified blocks header.
* @property {BlockHeight} blockHeight - height at which the block was verified to be included in the chain.
*/

/**
* @event TransactionReceived
* @group Events
*
* @property {TransactionHash} transactionHash - hash of the received transaction.
*/

/**
* @event TransactionVerified
* @group Events
*
* @property {TransactionHash} transactionHash - hash of the transaction that has been verified to be included in the chain.
* @property {BlockHeight} blockHeight     - height at which the transaction was verified to be included in the chain.
*/

/**
* TODO: Double-check the properties here.
* @event TransactionDoubleSpendDetected
* @group Events
*
* @property {TransactionHash} transactionHash - hash of the transaction that has been verified to be included in the chain.
* @property {doubleSpendProofHash} blockHeight     - height at which the transaction was verified to be included in the chain.
*/

/**
* Notification that the status of an address history might have changed.
*
* @event AddressUpdateReceived
* @group Events
*
* @property {Address} address       - address relevant to the update notification.
* @property {AddressStatus} status  - address status hash.
*/

/**
 * Export an event emitter for SPV related events.
 *
 * @ignore
 */
export const electrumEvents = new EventEmitter();

/**
 * Subscribes to block updates and emits events when the chain status updates.
 * @group Abstractions
 *
 * @emits BlockReceived
 * @emits BlockVerified
 * @emits ChainStatus
 */
export async function monitorBlockchain(): Promise<void>
{
	// Set up a subscription to monitor for blockchain header updates.
	await subscribeToBlockheaderUpdates();
}

/**
 * Subscribes to address updates and emits events when the address history changes.
 *
 * Note that any transaction that relates to the address history, will automatically be monitored as well.
 * @group Abstractions
 *
 * @emits AddressBalance?
 * @emits AddressHistory?
 *
 * @param address {Address} - The address to monitor for changes.
 */
export async function monitorAddress(address: Address): Promise<void>
{
	// Set up a subscription to monitor for the address for status updates.
	await subscribeToAddressUpdates(address);
}

/**
 * Subscribes to transaction updates and emits events when the transaction status changes.
 * @group Abstractions
 *
 * @emits TransactionReceived
 * @emits TransactionVerified
 * @emits TransactionDoubleSpendDetected
 *
 * @param transactionHash {TransactionHash} - The transaction to monitor for changes.
 */
export async function monitorTransaction(transactionHash: TransactionHash): Promise<void>
{
	// Set up a subscription to monitor for block inclusion.
	await subscribeToTransactionUpdates(transactionHash);

	// Also monitor for double-spend proofs for the transaction.
	await subscribeToDoublespendUpdates(transactionHash);
}

/**
 * Utility function to calculate a percentage for chain recovery status.
 * @ignore
 */
async function calculateChainRecoveryPercent(internalBlockHeight: BlockHeight, externalBlockHeight: BlockHeight): Promise<number>
{
	return ((internalBlockHeight - defaultCheckpoint.height) / (externalBlockHeight - defaultCheckpoint.height)) * 100;
}

/**
 * Handles incoming block headers by verifying their validity, storing them in a cache and emitting related events.
 * @ignore
 */
async function processBlockHeader(blockHeight: BlockHeight, blockHeader: BlockHeaderHex, targetBlockHeight: BlockHeight): Promise<void>
{
	// Calculate the full and shortened hash of the block header.
	const processedBlockheaderHash = await getBlockHashFromHeader(blockHeader);
	const shortenedBlockheaderHash = processedBlockheaderHash.replace(/^0+/, '');

	// Store block height to indicate the block header was successfully received.
	await storeInCache('chainState', 'currentHeight', blockHeight);

	// Send a signal indicating that we have received a new block.
	electrumEvents.emit('BlockReceived', { blockHeight, blockHash: processedBlockheaderHash });

	// TODO: Validate chainwork? (processBlockHeader is only called from subscription responses, which should always be above the checkpoint height)

	// Store the blocks header, height and hash in the local cache.
	await storeInCache('blockHeaders', shortenedBlockheaderHash, blockHeader);
	await storeInCache('blockheaderHeightToHash', blockHeight, shortenedBlockheaderHash);
	await storeInCache('blockheaderHashToHeight', shortenedBlockheaderHash, blockHeight);

	// Send a signal indicating that we have validated a new block.
	// TODO: Verify that this is all information we want to send here.
	electrumEvents.emit('BlockVerified', { blockHeight, blockHash: processedBlockheaderHash });

	// Store block height to indicate the block header was successfully verified.
	await storeInCache('chainState', 'verifiedHeight', blockHeight);

	// Calculate the current chain recovery percent after accepting the block as valid.
	const chainRecoveryPercent = Math.floor(await calculateChainRecoveryPercent(blockHeight, targetBlockHeight));

	// If the recovery state has changed significantly enough..
	if(chainRecoveryPercent !== currentChainStatus.verifiedPercent)
	{
		// Update the current chain status.
		currentChainStatus =
		{
			verifiedHeight: blockHeight,
			verifiedPercent: chainRecoveryPercent,
		};

		// Issue an event indicating that chain status has been updated.
		// TODO: Decide what numbers and names to emit.
		electrumEvents.emit('ChainStatus', currentChainStatus);
	}
}

/**
 * Handles incoming transaction updates by validating their block inclusion status and storing them in a cache and emitting related events.
 * @private
 */
async function processTransactionUpdate(transactionHash: TransactionHash, blockHeight?: BlockHeight): Promise<void>
{
	// Handle this transaction, if it has not been received before.
	if((typeof internalTransactionState[transactionHash] === 'undefined') || (internalTransactionState[transactionHash] < TRANSACTION_IS_RECEIVED))
	{
		// Get the transaction to ensure it gets cached.
		await getTransaction(transactionHash);

		// Emit event for the transaction, as we haven't seen it before.
		electrumEvents.emit('TransactionReceived', transactionHash);

		// Set the internal tracking of this transactions state to unverified.
		internalTransactionState[transactionHash] = TRANSACTION_IS_RECEIVED;
	}

	// NOTE: Double-spend state is between RECEIVED and VERIFIED, but is handled by processElectrumNotifications().

	// Verify this transactions block inclusion, if it has not been verified before.
	if((typeof internalTransactionState[transactionHash] === 'undefined') || (internalTransactionState[transactionHash] < TRANSACTION_IS_VERIFIED))
	{
		// Use the provided blockheight, or fetch the blockheight from the network if necessary.
		const someHeight = blockHeight || await fetchTransactionBlockHeight(transactionHash);

		// Skip block inclusion validation for this transaction as it is either unknown, or in the mempool.
		// NOTE: Electrum provides 'null' for unknown, and a height of '0' or less for in-mempool.
		if((someHeight === null) || (someHeight <= 0))
		{
			return;
		}

		// Get transactions block inclusion proof since it is not in the mempool.
		const { pos, merkle } = await getTransactionInclusionProof(transactionHash, blockHeight);

		// Get the blocks header in order to verify the transaction inclusion proof.
		const blockHeaderHex = await getBlockHeaderFromHeight(blockHeight);

		// Parse the blockheader to extract the merkle root.
		const root = await getMerkleRootFromHeader(blockHeaderHex);

		// Verify that the transaction is included in the block.
		await verifyTransactionInclusionProof(transactionHash, pos, merkle, root);

		// Emit event for validated transaction proof, if we haven't already?
		electrumEvents.emit('TransactionVerified', transactionHash);

		// Set the internal tracking of this transactions state to unverified.
		internalTransactionState[transactionHash] = TRANSACTION_IS_VERIFIED;
	}
}

/**
 * Handles incoming address updates by tracking the address history, unspent outputs and balances, storing transactions in a cache and emitting related events.
 * @private
 */
async function processAddressUpdate(address: Address): Promise<void>
{
	electrumEvents.emit('AddressUpdate', address);

	// TODO: Fetch history since last update (or all if this is the first request)
	const updatedAddressHistory = await fetchHistory(address) as AddressGetHistoryResponse;

	// Set up a list of empty transaction update promises.
	const transactionUpdatePromises = [];

	// Iterate over history to:
	for(const { tx_hash: transactionHash, height } of updatedAddressHistory)
	{
		// Monitor the transaction if it is still in the mempool.
		if(height <= 0)
		{
			// Request updates for this transaction in the future.
			// NOTE: In rare cases, this might monitor an already monitored transactions and that is not a problem.
			// NOTE: We do not await this in order to ensure fast processing transaction histories.
			monitorTransaction(transactionHash);
		}

		// Process the transaction as if it a transaction update event was emitted.
		// NOTE: We do not await this in order to ensure fast processing of transaction histories.
		transactionUpdatePromises.push(processTransactionUpdate(transactionHash, height));
	}

	// Wait for all transaction updates to complete.
	await Promise.all(transactionUpdatePromises);

	// TODO: Use the updated address data to:
	// - determine a current UTXO set for the address
	// - determine a current balance based on the UTXO set.
	//   · emit event at end of process if balance changed
}

/**
 * Requests block headers in a loop until the internal and external block height is synchronized.
 * @ignore
 */
async function synchronizeBlockHeaders(targetBlockHeight: BlockHeight, targetBlockheader: BlockHeaderHex): Promise<void>
{
	// Throw an error if the reported chain height is less than the checkpoint height.
	if(targetBlockHeight < defaultCheckpoint.height)
	{
		throw(new Error(`Network blockchain height (${targetBlockHeight}) is lower than the known-good checkpoint height (${defaultCheckpoint.height}).`));
	}

	// If synchronization is currently ongoing, do nothing.
	if(await headerSynchronizationLock.isLocked())
	{
		return;
	}

	// Lock the header synchronization lock to prevent concurrent synchronization of headers.
	const unlock = await headerSynchronizationLock.acquire();

	try
	{
		// Fetch the internal block height from cache.
		let internalBlockHeight = await getFromCache('chainState', 'currentHeight');

		// While there are block header to synchronize between our internal state and the provided block..
		while(internalBlockHeight < targetBlockHeight)
		{
			// Determine what the next block height will be.
			const startingBlockHeight = internalBlockHeight + 1;

			// Count the number of blocks there are to download.
			const remainingBlocks = targetBlockHeight - internalBlockHeight;

			// Batch-fetch up to 1000 headers from the checkpointHeight towards the current tip and process them.
			const blockHeaders = await fetchBlockHeaders(startingBlockHeight, Math.min(1000, remainingBlocks));

			// Process each of the new block headers
			for(const blockHeader of blockHeaders)
			{
				// Determine what the next block height will be.
				const nextBlockHeight = internalBlockHeight + 1;

				// Process the provided block header as the next in the chain.
				await processBlockHeader(nextBlockHeight, blockHeader, targetBlockHeight);

				// Bump the internal block height to the next block.
				internalBlockHeight = nextBlockHeight;
			}
		}

		// Process the provided block header, as if it was the most recent block in the chain.
		await processBlockHeader(targetBlockHeight, targetBlockheader, targetBlockHeight);
	}
	catch(error)
	{
		// TODO: Handle errors better.
	}
	finally
	{
		// Unlock the thread so further synchronization can happen.
		unlock();
	}
}

/**
 * Handler function for all electrum notifications.
 *
 * This function determines what type of notification has been provided and emits relevant signals.
 * For new block headers it also starts the process to validate and extend the chain tip.
 *
 * @private
 */
async function processElectrumNotifications(notification: ElectrumNotification): Promise<void>
{
	// If this is an address status update notification..
	if(notification.method === 'blockchain.address.subscribe')
	{
		// Extract the address and status from the notification.
		const { params } = notification as AddressSubscribeNotification;
		const [ address ] = params;

		// Trigger address update processing.
		processAddressUpdate(address);
	}

	// If this is a transaction status update notification..
	if(notification.method === 'blockchain.transaction.subscribe')
	{
		// Extract the transaction hash and status from the notification.
		const { params } = notification as TransactionSubscribeNotification;
		const [ transactionHash ] = params;

		// Process the transaction update.
		processTransactionUpdate(transactionHash);
	}

	// If this is a transaction double spend notification..
	if(notification.method === 'blockchain.transaction.dsproof.subscribe')
	{
		// Extract the double spend proof from the notification.
		const { params } = notification as TransactionDsProofNotification;
		const [ doubleSpendProof ] = params;

		// Update internal transaction state, but only if it has not been verified in a block.
		internalTransactionState[doubleSpendProof.txid] = Math.max(TRANSACTION_IS_DOUBLESPENT, internalTransactionState[doubleSpendProof.txid]);

		// Send a signal indicating that the provided transaction hash has been doublespent.
		// NOTE: Double spend proofs are taken into account for the transaction and address status updates, so no further action is necessary.
		electrumEvents.emit('TransactionDoubleSpendDetected', { transactionHash: doubleSpendProof.txid, doubleSpendProofHash: doubleSpendProof.dspid });
	}

	// If this is a block header notification..
	if(notification.method === 'blockchain.headers.subscribe')
	{
		// Extract the block header and height from the notification.
		const { params } = notification as HeadersSubscribeNotification;
		const { height, hex } = params[0];

		// Synchronize up to this block in the chain.
		synchronizeBlockHeaders(height, hex);
	}
}

/**
 * Initializes a light client that implements the Simple Payment Verification protocol transparently for the user.
 * @group Initialization
 *
 * @param application   {string}            name of the application to identify as with the servers.
 * @param transport     {TransportScheme}   which electrum transport to use when connecting with the servers.
 * @param servers       {ElectrumServers}   list of electrum backend servers to connect with.
 */
export async function initializeElectrum(application: string, transport: TransportScheme = 'wss', servers: ElectrumServers = defaultServers): Promise<void>
{
	// Create a connection with the electrum network.
	await initializeNetworkProvider(application, transport, servers);

	// Listen for notifications
	networkProvider.on('notification', processElectrumNotifications);

	// Resume validation from the current chain state.
	const internalBlockHeight = await getFromCache('chainState', 'currentHeight');

	// Reset to the checkpoint height if no chain state exist.
	if(!internalBlockHeight)
	{
		await storeInCache('chainState', 'currentHeight', defaultCheckpoint.height);
	}

	// TODO: Get and validate internal state (checkpoint, blockheight, chainwork etc)

	// Set up tracking for blockheader events.
	await monitorBlockchain();
}
