/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

/**
 * Name for a method that can be used to request data from an electrum server.
 * @ignore
 */
export type RequestMethod = string;

/**
 * Hash of data that relates to the current history and state of a blockchain address.
 * @ignore
 */
export type AddressStatus = string;

/**
 * Hash of data that relates to the current history and state of a blockchain lockscript.
 * @ignore
 */
export type ScriptHashStatus = string;

/**
 * Hash of data that relates to the current state of a blockchain transaction.
 * @ignore
 */
export type TransactionStatus = string;

/**
 * ...
 * @ignore
 */
export type DoubleSpendProofStatus = string;

/**
 *
 * @ignore
 */
export type TokenFilter = 'include_tokens' | 'exclude_tokens' | 'tokens_only';

/**
 *
 * @ignore
 */
export type VersionNumbers =
{
	major: number;
	minor: number;
	patch: number;
};
