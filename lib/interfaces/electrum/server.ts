/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

/**
 * A newly-started server uses this call to get itself into other servers’ peers lists. It should not be used by wallet clients.
 *
 * @memberof Blockchain.Server
 */
export type AddPeerRequest =
{
	/**  Must be: 'server.add_peer'. */
	method: 'server.add_peer';

	/** TODO: Document the features */
	features: any;
};

/**
 * A boolean indicating whether the request was tentatively accepted. The requesting server will appear in server.peers.subscribe() when further sanity checks complete successfully.
 *
 * @memberof Blockchain.Server
 */
export type AddPeerResponse = boolean;

/**
 * Return a banner to be shown in the Electrum console.
 *
 * @memberof Server
 */
export type BannerRequest =
{
	/**  Must be: 'server.banner'. */
	method: 'server.banner';
};

/**
 * A string.
 *
 * @memberof Blockchain.Server
 */
export type BannerResponse = string;

/**
 * Return a server donation address.
 *
 * @memberof Blockchain.Server
 */
export type DonationAddressRequest =
{
	/**  Must be: 'server.donation_address'. */
	method: 'server.donation_address';
};

/**
 * A string.
 *
 * @memberof Blockchain.Server
 */
export type DonationAddressResponse = string;

/**
 * Return a list of features and services supported by the server.
 *
 * @memberof Blockchain.Server
 */
export type FeaturesRequest =
{
	/**  Must be: 'server.features'. */
	method: 'server.features';
};

/**
 * A dictionary of keys and values. Each key represents a feature or service of the server, and the value gives additional information.
 *
 * @memberof Blockchain.Server
 */
export type FeaturesResponse = any;

/**
 * Return a list of peer servers.
 *
 * @note Despite the name this is not a subscription and the server will send no notifications.
 *
 * @memberof Blockchain.Server
 */
export type PeersSubscribeRequest =
{
	/**  Must be: 'server.peers.subscribe'. */
	method: 'server.peers.subscribe';
};

/**
 * An array of peer servers, each returned as a 3-element array.
 *
 * @memberof Blockchain.Server
 */
export type PeersSubscribeFeatures =
{
	[index: number]: string;
};
export type PeersSubscribeEntry =
{
	[index: number]: string | PeersSubscribeFeatures;
};
export type PeersSubscribeResponse = PeersSubscribeEntry;

/**
 * Ping the server to ensure it is responding, and to keep the session alive.
 *
 * @memberof Blockchain.Server
 */
export type PingRequest =
{
	/**  Must be: 'server.ping'. */
	method: 'server.ping';
};

/**
 * Returns null.
 *
 * @memberof Blockchain.Server
 */
export type PingResponse = null;

/**
 * Identify the client to the server and negotiate the protocol version.
 *
 * @note Only the first server.version() message is accepted.
 *
 * @memberof Blockchain.Server
 */
export type VersionRequest =
{
	/**  Must be: 'server.version'. */
	method: 'server.version';

	/** A string identifying the connecting client software. */
	client_name?: string;

	/** An array [protocol_min, protocol_max], each of which is a string. If protocol_min and protocol_max are the same, they can be passed as a single string rather than as an array of two strings, as for the default value. */
	protocol_version?: string | string[];
};

/**
 * An array of 2 strings: [server_software_version, protocol_version],
 * identifying the server and the protocol version that will be used for future communication.
 *
 * @memberof Blockchain.Server
 */
export type VersionResponseEntry =
{
	[index: number]: string;
};
export type VersionResponse = VersionResponseEntry;
