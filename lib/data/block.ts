// Import cache related functions.
import { getFromCache, storeInCache } from '../cache';

// Import network related functions.
import { fetchBlockHeaderWithProofFromBlockHeight } from '../network';

// Import utility functions.
import { verifyBlockInclusionProof } from '../chain';
import { getBlockHashFromHeader } from '../headers';

// Import types
import type
{
	BlockHeight,
	BlockHeaderHash,
	BlockHeaderHex,
} from '../interfaces';

// Import the checkpoint necessary to validate block inclusion.
import { electrumCheckpoint } from '@electrum-cash/checkpoint';

/**
 * @module ElectrumSPV
 */

/**
 * Gets a raw block header as a hex-encoded string, based on the provided block header hash.
 * @group Abstractions
 *
 * @param blockHeaderHash {BlockHeaderHash} - hash of the block header to use as an identifier.
 *
 * @returns {Promise<BlockHeaderHex>} the block header as a hex-encoded string.
 */
export async function getBlockHeader(blockHeaderHash: BlockHeaderHash): Promise<BlockHeaderHex>
{
	// Shorten the block header hash.
	const shortenedBlockheaderHash = blockHeaderHash.replace(/^0+/, '');

	// Lookup the block header in local cache.
	const cachedBlockHeaderHex = await getFromCache('blockHeaders', shortenedBlockheaderHash);

	// Return the block header from cache.
	if(cachedBlockHeaderHex)
	{
		return cachedBlockHeaderHex;
	}

	// Throw an error to indicate that the requested data is currently unavailable.
	throw(new Error(`Could not provide raw block header for ${blockHeaderHash}: Either the block does not exist in the indexer's chain, or the header has not yet been processed locally.`));
}

/**
 * Gets the block height where the provided block header hash was included in the chain.
 * @group Abstractions
 *
 * @param blockHeaderHash     {BlockHeaderHash}   hash of the block header to store.
 *
 * @returns {Promise<BlockHeaderHex>}
 */
export async function getBlockHeightFromHash(blockHeaderHash: BlockHeaderHash): Promise<BlockHeight>
{
	// Shorten the block header hash.
	const shortenedBlockheaderHash = blockHeaderHash.replace(/^0+/, '');

	// Lookup the block height in local cache.
	const cachedBlockHeight = await getFromCache('blockheaderHashToHeight', shortenedBlockheaderHash);

	// Return the block height from cache.
	if(cachedBlockHeight)
	{
		return cachedBlockHeight;
	}

	// Throw an error to indicate that the requested data is currently unavailable.
	throw(new Error(`Could not provide raw block header for ${blockHeaderHash}: Either the block does not exist in the indexer's chain, or the header has not yet been processed locally.`));
}

/**
 * Gets a raw block header as a hex-encoded string, based on the provided block height.
 * @group Abstractions
 *
 * @param blockHeight   {BlockHeight}   block height of the block
 *
 * @returns {Promise<BlockHeaderHex>}
 */
export async function getBlockHeaderFromHeight(blockHeight: BlockHeight): Promise<BlockHeaderHex>
{
	// Lookup the block header in local cache.
	const cachedBlockHeaderHash = await getFromCache('blockheaderHeightToHash', blockHeight);

	// Return the block header from cache.
	if(cachedBlockHeaderHash)
	{
		// NOTE: The cached header will be shortened with leading zeroes removed.
		//       These are not re-added here since this function would then need to remove them.
		return getBlockHeader(cachedBlockHeaderHash);
	}

	// Throw an error if we are unable to validate this header.
	if(blockHeight > electrumCheckpoint.height)
	{
		throw(new Error(`Could not validate a block header on height ${blockHeight}: The requested header is ahead of the current checkpoint at height ${electrumCheckpoint.height}`));
	}

	// Fetch the full block header and merkle proof for this height.
	// NOTE: the result of this network call is not cached, since it is expected to not be needed again in the future.
	const { branch, header, root } = await fetchBlockHeaderWithProofFromBlockHeight(blockHeight, electrumCheckpoint.height);

	// Hash the fetched block header
	const fetchedBlockHeaderHash = await getBlockHashFromHeader(header);
	const shortenedBlockheaderHash = fetchedBlockHeaderHash.replace(/^0+/, '');

	try
	{
		// Validate chain inclusion
		await verifyBlockInclusionProof(fetchedBlockHeaderHash, blockHeight, branch, root);

		// Store the block header.
		await storeInCache('blockHeaders', shortenedBlockheaderHash, header);
	}
	catch(error)
	{
		// Re-throw the error with more context.
		throw(new Error(`Failed to validate block header ${fetchedBlockHeaderHash} at height ${blockHeight}: validation of block inclusion in checkpoint at height ${electrumCheckpoint.height} failed.`));
	}

	// Store the block header in both orientations.
	// NOTE: The height->hash is considered immutable since the block inclusion proof for the trusted checkpoint passed.
	await storeInCache('blockheaderHeightToHash', blockHeight, shortenedBlockheaderHash);
	await storeInCache('blockheaderHashToHeight', shortenedBlockheaderHash, blockHeight);

	// Return the fetched block header.
	return header;
}
